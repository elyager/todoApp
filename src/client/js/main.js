document.addEventListener('DOMContentLoaded', function() {

  loadTasks();

  document.getElementById('js-addButton').addEventListener('click', function() {
    var taskInput = document.getElementById('js-newTask');
    var newTask = { content: taskInput.value};
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", '/api/tasks', true);
    xmlhttp.setRequestHeader("Content-type", "application/json");
    xmlhttp.onreadystatechange = requestStatus;
    xmlhttp.send(JSON.stringify(newTask));
    taskInput.value = '';

    //Erase list and regenerate with new Ids
    document.getElementById('todoList').innerHTML = '';
    loadTasks();
  });

  document.getElementById('js-newTask').addEventListener('keypress', function(e){
    if (e.keyCode === 13) {
      document.getElementById('js-addButton').click();
    }
  });

  document.getElementById("todoList").addEventListener("click", function(e) {
    var xmlhttp;
    var li = closest(e.target, function(el) {
      return el.tagName.toLowerCase() == 'li';
    });

    //Edit Task checkbox
    if (e.target && e.target.matches(".checkbox-item")) {
      var newTask = { _id: li.dataset.id, completed: e.target.checked};
      console.log(newTask);
      xmlhttp = new XMLHttpRequest();
      xmlhttp.open("POST", '/api/tasks/' + li.dataset.id, true);
      xmlhttp.setRequestHeader("Content-type", "application/json");
      xmlhttp.onreadystatechange = requestStatus;
      xmlhttp.send(JSON.stringify(newTask));
    }

    //Remove task
    if (e.target && e.target.matches(".action-button.icon-trash-empty")) {
      var taskToRemove = { id: li.dataset.id};
      xmlhttp = new XMLHttpRequest();
      xmlhttp.open("DELETE", '/api/tasks/' + li.dataset.id, true);
      xmlhttp.setRequestHeader("Content-type", "application/json");
      xmlhttp.onreadystatechange = requestStatus;
      xmlhttp.send(JSON.stringify(taskToRemove));
      this.removeChild(li);
  	}
  });
});

function loadTasks() {
  var OK_STATUS = 200;
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("GET", '/api/tasks');
  xmlhttp.responseType = 'json';
  xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == XMLHttpRequest.DONE) {
          if(xmlhttp.status == OK_STATUS){
            xmlhttp.response.map(function(item) {
              addTask(item._id, item.content, item.completed);
            });
          } else {
            console.log('Error: ' + xmlhttp.statusText );
          }
      }
  };
  xmlhttp.send();
}

function addTask(idTask, taskName, isChecked) {
  // if (taskName === '') return;
  console.log(idTask);
  console.log(taskName);
  console.log(isChecked);
  var todoListItem;

  if (isChecked) {
    todoListItem =
    '<li data-id='+ idTask +'>'+
      '<input checked class="checkbox-item" type="checkbox">'+
      '<label>'+ taskName +'</label>'+
      '<div class="todoList-actions">'+
        '<i class="action-button icon-trash-empty"></i>'+
      '</div>'+
    '</li>';
  }
  else {
    todoListItem =
    '<li data-id='+ idTask +'>'+
      '<input class="checkbox-item" type="checkbox">'+
      '<label>'+ taskName +'</label>'+
      '<div class="todoList-actions">'+
        '<i class="action-button icon-trash-empty"></i>'+
      '</div>'+
    '</li>';
  }
  document.getElementById('todoList').innerHTML += todoListItem;
}

function requestStatus() {
    var OK_STATUS = 200;
    if (this.readyState == XMLHttpRequest.DONE) {
        if(this.status == OK_STATUS){
          console.log(this.response);
        }else{
          console.log('Error: ' + this.statusText );
        }
    }
}

function closest(el, fn) {
    while (el) {
        if (fn(el)) return el;
        el = el.parentNode;
    }
}

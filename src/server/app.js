var mongoose   = require('mongoose');

var http       = require('http');
var path       = require('path');
var express    = require('express');
var bodyParser = require("body-parser");
var app        = express();
var server     = http.createServer(app);
var port       = process.env.PORT || 1987;

var router     = require('./routes');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

mongoose.connect('mongodb://localhost/todoApp?auto_reconnect');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('We are connected to MongoDB!');
});

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.use('/api', router);
app.use(express.static(__dirname));

server.listen(port, 'localhost');
server.on('listening', function() {
  console.log('Express server started on port %s at %s', server.address().port, server.address().address);
});

var router  = require('express').Router();
var Task       = require('../models');

router.use(function(req, res, next) {
    console.log('This happen with any request');
    next(); // ir a la siguiente ruta y no parar aqui
});

router.route('/tasks')

  .post(function(req, res) {
    var task = new Task();
    task.content = req.body.content;

    task.save(function(err) {
      if(err) res.send(err);
      res.json({message: 'Task Created!'});
    });
  })

  .get(function(req, res) {
    Task.find(function(err, tasks) {
      if(err) res.send(err);
      res.json(tasks);
    });
  });

router.route('/tasks/:_id')

  .get(function(req, res) {
      Task.findById(req.params._id, function(err, task) {
        if (err) res.send(err);
        res.json(task);
      });
  })

  .put(function(req, res) {
    console.log(req.params);
    Task.findById(req.params._id, function(err, task) {
      if (err) res.send(err);
      console.log(req.body);
      task.content = req.body.content;
      task.completed = req.body.completed;

      task.save(function(err) {
        if (err) res.send(err);
        res.json({message: 'Task updated'});
      });
    });
  })

  .post(function(req, res) {
    console.log(req.params);
    Task.findById(req.params._id, function(err, task) {
      if (err) res.send(err);
      console.log(req.body);
      task.completed = req.body.completed;

      task.save(function(err) {
        if (err) res.send(err);
        res.json({message: 'Task updated'});
      });
    });
  })

  .delete(function(req, res) {
        Task.remove({
            _id: req.params._id
        }, function(err, task) {
            if (err) res.send(err);
            res.json({ message: 'Task deleted' });
        });
    });

router.get('/', function(req, res){
  res.json({message: 'AYALE'});
});

module.exports = router;

var mongoose = require('mongoose');

var TaskSchema = new mongoose.Schema({
  content: {type: String, default: ''},
  completed: {type: Number, default: false}
});

module.exports =  mongoose.model('Task', TaskSchema);

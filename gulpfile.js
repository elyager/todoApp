var gulp          = require('gulp');
var browserSync   = require('browser-sync');
var pg            = require('gulp-load-plugins')();
var watching      = true;
var reload        = browserSync.reload;
var BROWSER_SYNC_RELOAD_DELAY = 500;

gulp.task('sass', function() {
  gulp.src('src/client/scss/*.scss')
    .pipe(pg.sourcemaps.init())
    .pipe(watching ? pg.plumber() : pg.gutil.noop())
    .pipe(pg.sass())
    .pipe(pg.autoprefixer('last 2 version'))
    .pipe(pg.sourcemaps.write())
    .pipe(gulp.dest('dist/css'));
});

gulp.task('templates', function() {
  var YOUR_LOCALS = {};

  gulp.src('src/client/views/*.jade')
  .pipe(watching ? pg.plumber() : pg.gutil.noop())
  .pipe(pg.jade({
    locals: YOUR_LOCALS
    ,pretty:true
  }))
  .pipe(gulp.dest('dist/'))
  .pipe(browserSync.reload({
     stream: true
  }));
});

gulp.task('js', function(){
  gulp.src('src/client/js/*.js')
  .pipe(pg.sourcemaps.init())
  .pipe(pg.jshint())
  .pipe(pg.jshint.reporter('default'))
  .pipe(pg.concat('main.js'))
	.pipe(pg.uglify())
  .pipe(watching ? pg.plumber() : pg.gutil.noop())
  .pipe(pg.sourcemaps.write())
  .pipe(gulp.dest('dist/js'))
  .pipe(browserSync.reload({
     stream: true
  }));
});

gulp.task('copy-img', function(){
  gulp.src('src/client/images/*')
  .pipe(gulp.dest('dist/images'));
});

gulp.task('copy-font', function(){
  gulp.src('src/client/font/*')
  .pipe(gulp.dest('dist/font'))
  .pipe(browserSync.reload({
     stream: true
  }));
});

gulp.task('copy-app', function(){
  gulp.src('src/server/**/*.js')
  .pipe(gulp.dest('dist/'))
  .pipe(browserSync.reload({
     stream: true
  }));
});


gulp.task('nodemon', function (cb) {
  var called = false;
  return pg.nodemon({
    script: 'dist/app.js',
    watch: ['dist/app.js']
  })
  .on('start', function onStart() {
    if (!called) { cb(); }
    called = true;
  })
  .on('restart', function onRestart() {
    setTimeout(reload, BROWSER_SYNC_RELOAD_DELAY);
  });
});

gulp.task('browser-sync', ['copy-app', 'nodemon'], function () {
  browserSync.init({
    proxy: 'http://localhost:1987',
    port: 4000
  });
});

gulp.task('default', ['copy-font', 'copy-img', 'sass', 'js', 'templates', 'browser-sync'], function() {
  gulp.watch('src/client/images/*', ['copy-img']);
  gulp.watch('src/client/scss/*.scss', ['sass']);
  gulp.watch('src/client/js/*.js', ['js']);
  gulp.watch('src/server/*.js', ['copy-app']);
  gulp.watch('src/client/views/*.jade', ['templates']);
});
